import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {
  visible: boolean = false;
  value !: string;

  showDialog() {
    this.visible = true;
  }

}
