import {Recipe} from "./recipes.model";
import {EventEmitter, Injectable} from "@angular/core";
import {Ingredient} from "../shared/ingredient.model";
import {ShoppingListService} from "../shopping-list/shopping-list.service";

@Injectable()
export class RecipesService{
  recipeSelected = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
    new Recipe(
      'A test Recipe',
      'This is an empty recipe, dont work',
      'https://upload.wikimedia.org/wikipedia/commons/c/c1/Indian-Food-wikicont.jpg',
      [
        new Ingredient("Meat", 1),
        new Ingredient("French fries", 20)
      ]
    ),

    new Recipe(
      'Another Recipe',
      'This is an empty recipe, dont work',
      'https://www.shutterstock.com/shutterstock/photos/370298699/display_1500/stock-photo-notepad-for-your-recipe-with-herbs-and-spices-over-black-stone-background-top-view-with-copy-space-370298699.jpg',
      [
        new Ingredient("Buns", 2),
        new Ingredient("Meat", 1)
      ]
    ),
  ];

  constructor(private slService: ShoppingListService) {
  }

  getRecipes(){
    return this.recipes.slice();
  }

  addIngredientsToSHoppingList(ingredients : Ingredient[]){
    this.slService.addIngredientsM(ingredients);
  }
}
